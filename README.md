#### PhantomJS Version:
1.9.1  

#### PhantomJS:
[http://phantomjs.org/](http://phantomjs.org/)  

#### Usage:
phantomjs chatwork-bot.js <email> <password> <room name> <message> [environment]  

#### Sample:
phantomjs chatwork-bot.js email@gmail.com 123456789 "HOGEHOGE ROOM" "HELLO EVERYONE" live  

#### Environment:
live > User uses Chatwork Live as the default environment  
classic > User uses the old Chatwork environment (DEFAULT)  

#### Reference:
[https://github.com/ento/chatwork-bot](https://github.com/ento/chatwork-bot)  
[https://github.com/ha1t/chatwork-bot](https://github.com/ha1t/chatwork-bot)  
