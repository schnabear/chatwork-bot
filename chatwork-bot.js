var page = require('webpage').create();
var system = require('system');
var loadInProgress = false;

var options = {
    email: { index: 1, value: "", optional: false },
    password: { index: 2, value: "", optional: false },
    roomName: { index: 3, value: "", optional: false },
    message: { index: 4, value: "", optional: false },
    environment: { index: 5, value: "", default: "classic", optional: true },
};

if (system.args.length < 5) {
    printHelp();
    phantom.exit(1);
}

for (var o in options) {
    var arg = system.args[options[o].index];
    if (options[o].optional && arg == undefined) {
        options[o].value = options[o].default;
    } else {
        options[o].value = arg;
    }
}

// page.settings.userAgent = null;
// page.navigationLocked = true;

page.onAlert = function(msg) {
    console.log('ALERT: ' + msg);
};

page.onConsoleMessage = function (msg) {
    console.log(msg);
};

page.onLoadStarted = function() {
    loadInProgress = true;
    var currentUrl = page.evaluate(function() {
        return window.location.href;
    });

    console.log("Page load started at " + currentUrl);
};

page.onLoadFinished = function(status) {
    loadInProgress = false;
    console.log('Status: ' + status);
};

page.onResourceRequested = function(requestData, networkRequest) {
    //console.log('Request (#' + requestData.id + '): ' + JSON.stringify(requestData));
};

page.onResourceReceived = function(response) {
    //console.log('Response (#' + response.id + ', stage "' + response.stage + '"): ' + JSON.stringify(response));
};

page.onResourceError = function(resourceError) {
    console.log('Unable to load resource (URL:' + resourceError.url + ')');
    console.log('Error code: ' + resourceError.errorCode + '. Description: ' + resourceError.errorString);
};

page.onNavigationRequested = function(url, type, willNavigate, main) {
    console.log('Trying to navigate to: ' + url);
    console.log('Caused by: ' + type);
    console.log('Will actually navigate: ' + willNavigate);
    console.log('Is main frame: ' + main);
}

page.onError = function(msg, trace) {
    var msgStack = ['ERROR: ' + msg];

    if (trace) {
        msgStack.push('TRACE:');
        trace.forEach(function(t) {
            msgStack.push(' -> ' + t.file + ': ' + t.line + (t.function ? ' (in function "' + t.function + '")' : ''));
        });
    }

    console.error(msgStack.join('\n'));

    phantom.exit(1);
};

var CW = {
    currentStep: 1,
    maxStep: 5,
    isRunning: false,
    email: '',
    password: '',
    roomName: '',
    message: '',
    environment: 'live',
    url: '',
    execute: function() {
        this.isRunning = true;
        var interval = setInterval(function() {
            if (!loadInProgress && CW.currentStep <= CW.maxStep) {
                console.log('Step: ' + CW.currentStep);

                switch (CW.currentStep) {
                    case 1:
                        CW.open(CW.url);
                        break;
                    case 2:
                        CW.login();
                        break;
                    case 3:
                        CW.gotoRoom();
                        break;
                    case 4:
                        CW.postMessage();
                        break;
                    case 5:
                        CW.waitChatSend();
                        break;
                }

                CW.currentStep++;
            }
            if (!loadInProgress && CW.currentStep > CW.maxStep) {
                console.log('Execution complete');
                clearInterval(interval);
                CW.isRunning = false;
            }
        }, 1000);
    },
    open: function(url) {
        page.open(url, function() {
            var hasJQuery = page.evaluate(function() {
                return typeof jQuery === 'function';
            });

            if ( !hasJQuery ) {
                console.log('jQuery not found...');
                phantom.exit(1);
            }
        });
    },
    login: function() {
        page.evaluate(function(args) {
            var email = $('input[name="email"]');
            var password = $('input[name="password"]');
            var login = $('input[name="login"]');

            email.val(args[0]);
            password.val(args[1]);
            login.click();
        }, [this.email, this.password]);
    },
    gotoRoom: function() {
        var roomNavigation = page.evaluate(function(args) {
            var roomLink;

            switch (args[0]) {
                case 'live':
                    var roomLinks = $('._roomLink');
                    var nameContainer = 'p.chatListTitleArea';
                    break;
                case 'classic':
                default:
                    var roomLinks = $('.cw_room_link');
                    var nameContainer = 'div.ui_room_title';
                    break;
            }

            roomLinks.each(function() {
                if ($(nameContainer, this).text() == args[1]) {
                    console.log('Found room: ' + args[1]);
                    roomLink = $(this);
                    return false;
                }
            });

            if (roomLink == undefined) {
                console.log('Room not found...');
                return false;
            } else {
                roomLink.click();
            }

            return true;
        }, [this.environment, this.roomName]);

        if (!roomNavigation) {
            phantom.exit(1);
        }
    },
    postMessage: function() {
        page.evaluate(function(args) {
            switch (args[0]) {
                case 'live':
                    var textarea = $('#_chatText');
                    var send = $('#_sendButton');
                    break;
                case 'classic':
                default:
                    var textarea = $('#cw_chattext');
                    var send = $('#cw_send_button');
                    break;
            }

            textarea.focus();
            textarea.val(args[1]);
            send.click();
        }, [this.environment, this.message]);
    },
    waitChatSend: function() {
        var isSending = page.evaluate(function(args) {
            switch (args[0]) {
                case 'live':
                    var textarea = $('#_chatText');
                    break;
                case 'classic':
                default:
                    var textarea = $('#cw_chattext');
                    break;
            }

            if (textarea.val() != '') {
                return true;
            }

            return false;
        }, [this.environment]);

        if (isSending) {
            CW.currentStep--;
        }
    },
};

function printHelp() {
    var help = "Usage: phantomjs %prog ";
    var sorted = [];
    for (var o in options) {
        if (options[o].optional) {
            sorted[options[o].index] = '[' + o + ']';
        } else {
            sorted[options[o].index] = o;
        }
    }
    for (var i in sorted) {
        help += sorted[i] + " ";
    }
    console.log(help);
}

CW.url = 'https://kcw.kddi.ne.jp';
CW.email = options.email.value;
CW.password = options.password.value;
CW.roomName = options.roomName.value;
CW.message = options.message.value;
CW.environment = options.environment.value;
CW.execute();
var interval = setInterval(function() {
    if (!CW.isRunning) {
        phantom.exit(0);
    }
}, 1000);
